
let mapOptions = {
    center:[45.7805, 15.9882],
    //center:L.control.locate,
    zoom:10
}

let map = new L.map('map', mapOptions);

let layer = new L.TileLayer('http://tile.openstreetmap.org/{z}/{x}/{y}.png');
map.addLayer(layer);
