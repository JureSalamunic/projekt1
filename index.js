const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
const { auth } = require('express-openid-connect');
var https = require('https')
var http = require('http')
var fs = require('fs')

var indexRouter = require('./routes/home');
var app = express()
  .use(express.static(path.join(__dirname, 'public')))
  .use(
    auth({
      auth0Logout:true,
      issuerBaseURL: 'https://dev-milgssq9.us.auth0.com',
      baseURL: 'https://localhost:'+PORT,
      clientID: 'zRUTsyfsHIXUDNzKnO0G1TDiFQXOLyJU',
      secret: 'h_tfEk8KaQqRnQXKj-ZqSFObvYwakiurFhIdjG5F0o8qpKKY83M0Jdx4lw8e4lcA',
      idpLogout: true,
    })
  )
  .use('/', indexRouter)
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs');

  //.listen(PORT, () => console.log(`Listening on ${ PORT }`))
  
  var server = https.createServer({
    key: fs.readFileSync('cert/key.pem'),
    cert: fs.readFileSync('cert/cert.pem')
  },app).listen(PORT, () => console.log(`Listening on ${ PORT }`));

//var server = http.createServer(app).listen(PORT, () => console.log(`Listening on ${ PORT }`));

